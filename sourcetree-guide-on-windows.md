# Source Tree 使用手册

**READ ME FIRST for a User's Guide to install git and generate a new SSH key on windows.** [**click to read**][install-git]



## 一、基础设置

- 菜单栏：工具 -> 选项

### 1. 基本信息配置

![git-base-settings](http://ww4.sinaimg.cn/large/006tKfTcgy1ffohww1k2cj30ic0l4tbz.jpg)

**注：SSH 密钥的路径，为 [生成密钥对（点我查看）][ssh-keygen] 时保存的路径。**



### 2. 开启 Git 服务

![enable-git](http://ww4.sinaimg.cn/large/006tNc79gy1ffohzjkjt9j30ic0l4wgo.jpg)



## 二、Git 基本操作

### 1. 仓库

![clone](http://ww2.sinaimg.cn/large/006tKfTcgy1ffohwtyes1j30l50gjmza.jpg)

- Clone：克隆远端仓库到本地

- Add：添加本地仓库到 SourceTree 书签，即本地已有仓库，使用 SourceTree 进行管理

- Create：新建本地仓库

  ​



### 2. Panels

![workspace](http://ww1.sinaimg.cn/large/006tKfTcgy1ffohxkja5rj30yo0njn4r.jpg)

成功克隆远端仓库，SourceTree 跳转到项目的工作界面，如上图所示。我将其分为 6 Panels。



#### 1区

- 文件状态（工作副本）

  ![workdir](http://ww3.sinaimg.cn/large/006tKfTcgy1ffohwsmiapj30xp0l4wj6.jpg)

  将要文件从 `未暂存文件` 添加到 `已暂存文件`，按照规范书写 `commit message`，点击右下角的`提交`，将修改提交到本地的 `commit history`。



- 分支

  ![branch](http://ww4.sinaimg.cn/large/006tNc79gy1ffoi07tsaej30xp0l4wlq.jpg)

  将当前变更衍合到 master：即 **rebase(变基/衍合)** 操作，禁止在公共分支使用 `rebase` 操作。有兴趣可自行了解 `rebase` 命令。



- 标签

  ![tag](http://ww4.sinaimg.cn/large/006tKfTcgy1ffohwurrqqj30ku06e75e.jpg)



- 远程

  以下 3 张图片，向您展示，如何拉取远端分支到本地仓库：

  1. 如图所示，在`远程`选项中，选中需要拉取的分支，右键，在弹出的窗口选择`检出 origin/分支名...`

  ![create-local-branch-based-on-remote-branch_1](https://ws4.sinaimg.cn/large/006tNbRwgy1fgg24x2q7tj30o80fsdi2.jpg) 

  2. 点击`检出 origin/分支名...`之后，弹出窗口，`检出远程分支`、`新的本地分支`和`本地分支应跟踪远程分支` 3 个选项使用默认值即可，然后点击确认。

  ![create-local-branch-based-on-remote-branch_3](https://ws1.sinaimg.cn/large/006tNbRwgy1fgg24t90z3j30wg0cmmzc.jpg)

  3. 上一步`确认`之后，本地分支创建成功。 `feature/awesome-git` 和 `originfeature/awesome-git` 指向同一个 `commit` 点。至此，拉取远程分支到本地仓库的操作已完成。

  ![create-local-branch-based-on-remote-branch_2](https://ws4.sinaimg.cn/large/006tNbRwgy1fgg24qqleaj30n60dxgnp.jpg)



#### 2区

- 推送

  ![push](http://ww1.sinaimg.cn/large/006tKfTcgy1ffohwt7q0vj30pe0jmjv7.jpg)

  当 remote repository 有新的 commits 未拉取到本地，提交失败。需要 pull 新的 commits 到本地（如有冲突，则需要解决冲突），再执行 push 操作。

​

- 拉取

  ![pull](http://ww2.sinaimg.cn/large/006tKfTcgy1ffohwviasjj30qv0buwgi.jpg)

  图中的四个选项，没理解所表达的意思。如果有理解的童鞋，麻烦联系我。谢谢！



- 获取

  ![fetch](http://ww1.sinaimg.cn/large/006tKfTcgy1ffohxlyq8gj30n509r402.jpg)

  - 从全部远端获取更新：管理多远端仓库时，才有作用。默认勾选即可。
  - 删掉所有远端现已不存在的跟踪分支：远端分支已被删除，是否也删除本地的分支
  - 获取所有标签：远端存在而本地不存在的标签，如小伙伴新提交的 tags

  ​

  **拉取 `pull` 与获取 `fetch` 的区别**

  - git pull：从远程获取最新版本并 merge 到本地
  - git fetch：从远程获取最新版本到本地，不会自动 merge

  上述命令其实相当于 `git fetch` 和 `git merge`。在实际使用中，`git fetch` 更安全一些。因为在 `merge` 前，我们可以查看更新情况，然后再决定是否合并。

  ​

- 分支

  - 创建分支

    ![create-branch](http://ww1.sinaimg.cn/large/006tKfTcgy1ffohwu9wrbj30qb0aymze.jpg)

  - 删除分支

    ![delete-branch](http://ww3.sinaimg.cn/large/006tKfTcgy1ffohxlcslkj30pb0cgjth.jpg)

  ​

- 合并

  ![merge](http://ww2.sinaimg.cn/large/006tKfTcgy1ffohwsrtxrj30xp0l4n27.jpg)

  合并指定的 commit 到当前分支，比如切换到 master 分支进行合并 develop 分支的某个 commit_id。如果指定的 commit_id 刚好是分支指向的提交点，则是合并整个分支。

  ​

- 贮藏

  - 贮藏

    ![stash](http://ww2.sinaimg.cn/large/006tKfTcgy1ffohws0a53j30xp0l4774.jpg)

    保存暂存的更改：勾选时，被添加到 index/stage 的文件，贮藏的同时会保留 index/stage 的修改。一般**不勾选**。

  - 应用/删除贮藏

    ![stash-apply-drop](http://ww2.sinaimg.cn/large/006tNc79gy1ffoi0w21hdj30h10e6jt6.jpg)

  ​

- 丢弃

  ![discard](http://ww2.sinaimg.cn/large/006tKfTcgy1ffohwrhlmmj30xp0l4q5q.jpg)

​

- 标签

  - 新建

    ![create-tag](http://ww1.sinaimg.cn/large/006tKfTcgy1ffohxlakqbj30ti0bnacn.jpg)

  - 删除

    ![delete-tag](http://ww2.sinaimg.cn/large/006tKfTcgy1ffohxk51ojj30pq09cwg0.jpg)

    ​

#### 3区

- Git 工作流

  ![git-flow](http://ww2.sinaimg.cn/large/006tNc79gy1ffojgh1cp1j30xp0cqq69.jpg)

  使用 `Git Flow` 初始化仓库，使用默认配置即可。

  ![git-flow-detail](http://ww4.sinaimg.cn/large/006tNc79gy1ffojgtgbs6j30rv0n5aej.jpg)

  当执行分支合并操作时，默认情况，合并完成后当前分支会被删除。但有些情况，我们并**不希望删除当前分支**。

  ![keep-branch](https://ws1.sinaimg.cn/large/006tNc79gy1fgcgso2zl8j30xg0niwid.jpg)

  ​



#### 4区

- 提交历史


- 针对某个 commit_id 操作

  ![commit-oper](http://ww3.sinaimg.cn/large/006tNc79gy1ffojh7p50tj30ve0ijjwg.jpg)

  - `检出`、`合并`、`标签`、`分支` 操作参考前面的内容，由分支变换为指定 `commit` 进行操作。
  - `存档`：以当前 `commit` 历史，打包压缩归档。
  - 衍合/交互式衍合：非本文档学习内容。
  - 重置当前分支到此次提交，即 `git reset --hard/mixed/soft commit_id`（该命令作用非本文档知识点）。
  - 回滚提交，即 `revert` 操作，创建一个新的提交点，来回滚当前提交点的操作。**推荐的回滚方式**。
  - 创建补丁，补丁知识自行学习。

  ​

#### 5区

commit 的详细信息



#### 6区

文件修改对比





[install-git]: http://git.oschina.net/buildyourdream/git-training-supplement/blob/master/install-git-on-windows.md
[ssh-keygen]: http://git.oschina.net/buildyourdream/git-training-supplement/blob/master/install-git-on-windows.md#%E7%94%9F%E6%88%90-ssh-%E5%AF%86%E9%92%A5%E5%AF%B9

