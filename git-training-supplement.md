### 场景一：
产品经理有两个需求，“QQ 音乐合作活动” 和 “登录流程优化”。  
小明和小花开开心心领完需求后，然后非常愉快地进入了开发状态。  
下班前，小花的功能开发完毕，将代码合并回了 `develop` 分支。  
小明看到 `develop` 分支有了新的提交，防止自己开发完成后合并时出现 `big bang conflict`，便想将 `develop` 分支新的提交点，同步到自己的“QQ 音乐活动”分支。  

#### 解决方案：
```
$ git checkout feature/QQMusicActivity
$ git rebase develop
```

#### 命令解释
上面命令的作用是：对分支 `feature/QQMusicActivity`，基于 `develop` 分支执行变基操作。

#### 后记：
在软件开发的世界里，如果一件事很痛苦，那就频繁地去做它。例如代码合并，集成和部署。
频繁地从 `develop` rebase 回 `feature branch`，能大大减轻 `big bang conflict` 的 `merge` 带来的痛苦。

---

### 场景二：
两个月过去了，“QQ 活动音乐活动”终于开发完成了，小明看了下私有的分支 `feature/QQMusicActivity` 提交历史，想将几个提交点合并成为一个提交点。

#### 解决方案：

```
git rebase -i -p commit_id
```

命令回车后，出现下面的提示界面：

```
pick 39695eb add alipay
pick 49ff312 add no.ff
pick 084081b Merge branch 'test-no-ff'

Commands:  
p, pick = use commit  
r, reword = use commit, but edit the commit message  
e, edit = use commit, but stop for amending  
s, squash = use commit, but meld into previous commit  
f, fixup = like "squash", but discard this commit's log message  
x, exec = run command (the rest of the line) using shell  
d, drop = remove commit  

These lines can be re-ordered; they are executed from top to bottom.  

If you remove a line here THAT COMMIT WILL BE LOST.  

However, if you remove everything, the rebase will be aborted.  

Note that empty commits are commented out  
```

#### 命令解释
将 `49ff312` 和 `084081b ` 合并到 `39695eb `

```
pick 39695eb add alipay  
s 49ff312 add no.ff  
s 084081b Merge branch 'test-no-ff'  
```
将 *pick* 修改为 *s* 或 *squash*，其余选项作用参照上述*Commands*列表


>
> git rebase -i -p commit\_id：
> commit\_id 是 提交点`39695eb`的父亲提交点。

-----

### 场景三
小明在当前分支上已经工作一段时间，所有的东西都进入了混乱的状态。  
这个时候产品经理小芳跑过来，需求有一点点小变更，让小明帮忙处理下。  
这个需求的更新，需要让小时切换到另一个分支。  
问题是，小明不想仅仅因为过会儿回到这一点而为做了一半的工作创建一次提交。  
针对这样的场景，小明使用 `git stash` 命令来贮藏代码。

```
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   bar.py
```

切换分支之前，小明需要储藏代码，运行 `git stash` 或 `git stash save "save message"`：

```
$ git stash save "xiaofang owes me one"
Saved working directory and index state On master: xiaofang owes me one
HEAD is now at 772faf5 foo
```

处理完事情后，小明想重新恢复刚才的工作现场，查看一下已储藏代码的列表：

```
$ git stash list
stash@{0}: On master: say hello
stash@{1}: On master: xiaofang owes me one
```

小明刚才储藏了两次工作。最近一次是 `stash@{0}` ，如果想恢复 `stash@{0}` ，可以执行 `git stash apply` 。如果不指定一个储藏，Git 默认是最近的储藏。如果想要应用其中一个更旧的储藏，就得指定它的名字，像这样：`git stash apply stash@{1}`。

```
$ git stash apply stash@{1}
```

恢复后，小明很惊讶，我确实肯定一定将 `bar.py` 文件添加到暂存区的了，为什么使用 `git status` 查看，却是显示 **new file:   bar.py**。


```
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   bar.py
```


文件的改动被重新 `apply` 了，但是之前暂存的文件却没有重新暂存。
想要那样的话，必须使用 `–-index` 选项来运行 `git stash apply` 命令，来尝试重新应用暂存的修改。


```
$ git stash apply --index
```

继续查看下储藏列表

```
$ git stash list  
stash@{0}: On master: say hello  
stash@{1}: On master: xiaofang owes me one  
```

噫，怎么还保留着已经恢复的 `stash` 呢？
原来，`git stash apply` 命令是需要恢复之后，手工去删除相关的 `stash`:


```
$ git stash drop stash@{0}
```

如果想在恢复完成后自动删除，使用

```
$ git stash pop
```

有了刚才的经验，小明不希望 git 储藏已经 `git add` 添加到暂存区的改动：

```
$ git stash --keep-index
# 它告诉 Git 不要储藏任何你通过 git add 命令已暂存的东西。
# 当你做了几个改动并只想提交其中的一部分，过一会儿再回来处理剩余改动时，这个功能会很有用。
```

经过几天的熟悉，小明发现了其中的规律，默认情况下，`git stash` 只会储藏已经在索引中的文件。但是他就是想储藏未被 git 跟踪的文件。

```
$ git stash -u
--include-untracked 或 -u 标记，Git 会储藏任何未被跟踪文件。
```


小明刚才将 `bar.py` 文件储藏，现在还不想恢复它到 `develop` 分支，但是又要在 `bar.py` 文件上修改一个地方。  
如果直接在 `develop` 分支上修改 `bar.py` 文件，那 `apply stash` 恢复时，`bar.py` 会有一个合并冲突，不得不去解决它。  
可是他好讨厌冲突，这该如何是好？  

这时小明突发奇想，我能不能使用储藏的代码创建一个新的分支呢？  
在新创建的分支 `stash-branch` 上修改 `bar.py` ，需要 `apply stash` 恢复时，直接合并 `stash-branch` 分支到 `develop`，而对储藏的代码，抛弃就好。

```
$ git stash branch branch-from-stash
```


-------



### 课后练习：
- [掌握] .gitignore
- [了解] git cherry-pick



> 不需要从头写 `.gitignore` 文件，这里有各种各样的配置文件: [click me] [gitignore-repo]


# 注意：
- **git rebase** 的所有操作，**绝不允许**对公共分支进行操作。因为它会改变当前分支的历史记录。
- `long lived branch` 是一种不好的实践，我们要尽量避免。






[gitignore-repo]: https://github.com/github/gitignore  "ignore template"








