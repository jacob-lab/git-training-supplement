

## Tab Autocompletion for Git Commands

Put the following lines in your `~/.bashrc`

```shell
if [ -f /etc/bash_completion.d/git ]; then
  . /etc/bash_completion.d/git
fi
```



Now you need to `source ~/.bashrc` to enable git auto-completion, and after you type `git c` then hit `TAB` key you can see all the git commands that start with *c* letter.

```shell
[root@localhost ~]# source ~/.bashrc
[root@localhost ~]# git c
checkout      cherry        cherry-pick   clean         clone         commit        config
```



