## 前言

本文档仅适用于 **windows** 平台



## 问题

安装 SourceTree 过程，会被**强制登录**，否则不能使用。但是由于特殊的网络环境，正常情况下，注册 SourceTree 或使用 G+ 账号，会变得异常困难，因此我们需要优雅跳过登录授权操作。

![sourcetree-login](http://ww2.sinaimg.cn/large/006tNbRwly1ffin8s80jhj30k00c8q44.jpg)



## 解决方案

* 进入用户本地文件夹下的 SourceTree 目录 `%LocalAppData%`
* 新建目录结构 `Atlassian/SourceTree` (系统自动创建目录，则跳过此步骤)
* 新建 `accounts.json` 文件



![LocalAppData](http://ww1.sinaimg.cn/large/006tNbRwly1ffin8tbtfwj30eb02c74b.jpg)

地址栏输入 `%LocalAppData%`，回车后跳转用户本地的 `Local` 目录

![sourcetree-accounts-json](http://ww2.sinaimg.cn/large/006tNbRwgy1ffinby8sbsj30k80f5ado.jpg)



`accounts.json`文件内容：

```json
[
  {
    "$id": "1",
    "$type": "SourceTree.Api.Host.Identity.Model.IdentityAccount, SourceTree.Api.Host.Identity",
    "Authenticate": true,
    "HostInstance": {
      "$id": "2",
      "$type": "SourceTree.Host.Atlassianaccount.AtlassianAccountInstance, SourceTree.Host.AtlassianAccount",
      "Host": {
        "$id": "3",
        "$type": "SourceTree.Host.Atlassianaccount.AtlassianAccountHost, SourceTree.Host.AtlassianAccount",
        "Id": "atlassian account"
      },
      "BaseUrl": "https://id.atlassian.com/"
    },
    "Credentials": {
      "$id": "4",
      "$type": "SourceTree.Model.BasicAuthCredentials, SourceTree.Api.Account",
      "Username": "",
      "Email": null
    },
    "IsDefault": false
  }
]
```



重新打开 SourceTree，直接显示主窗口



**参考资料：**

[SourceTree 免登录跳过初始设置](http://www.cnblogs.com/xiofee/p/sourcetree_pass_initialization_setup.html)



