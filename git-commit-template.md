



![code-quality-measurement](http://ww2.sinaimg.cn/large/006tNbRwly1ffidwba4wqj30fa0c1my7.jpg)



## Git Log 之痛

```
fix     => 这到底是 fix 什么？为什么 fix？怎么 fix 的？
update  => 更新了什么？是为了解决什么问题？
test    => 这个最让人崩溃，难道是为了测试？至于为了测试而去提交一次代码么？
```



## 5W1H

谁（who）在什么时候（when）、什么地点（where）因为什么（why）而做了什么事情（what），他是怎么做的（how）



## Template

```
What: 简短的描述这次的改动

Why:

* 为什么修改？就是要说明这次改动的必要性，可以是需求来源，任务卡的链接，或者其他相关的资料

How:

* 做了什么修改？需要说明的是使用了什么方法（比如数据结构、算法）来解决了哪个问题
* 本次修改的副作用可能有什么

# 50-character subject line
#
# 72-character wrapped longer description.
```



## Automation

`~/.gitconfig` or `.git/config`

```
[commit]
  template = ~/.gitmessage
```















参考文章：

[如何写出高可读的 commit message](https://juejin.im/post/59110c322f301e0057e4c182)