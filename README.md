Git 学习资料（**吐血推荐**）：

1. [git 简明指南][git-guide]
2. [廖雪峰老师 git 教程][git-tutorial-by-lxf]



**[下载 Git && SourceTree][download-git-st] ，提取密码: 87jk**



以下文档存在依赖关系，建议阅读顺序：

1. install-git-on-windows.md
2. git-FAQ.md
3. sourcetree-login-without-account-on-windows.md
4. sourcetree-guide-on-windows.md
5. resolving-merge-conflicts-in-git.md







|                   文件名                    |               描述                |
| :--------------------------------------: | :-----------------------------: |
|           ~~git-training.pdf~~           |  git 培训 ppt（***deprecated***）   |
|       version-naming-convention.md       |             版本号命名规范             |
|     git-commit-message-convention.md     |           git 提交信息规范            |
|        git-training-supplement.md        |           git 培训补充知识点           |
|              svn-to-git.pdf              |         svn 项目迁移 git 指南         |
|          git-commit-template.md          |           git 提交信息模板            |
|        install-git-on-windows.md         |       windows 平台安装 git 指南       |
| sourcetree-login-without-account-on-windows.md | windows 平台优雅跳过 SourceTree 的强制登录 |
|      sourcetree-guide-on-windows.md      |  SourceTree 使用小手册（windows 平台）   |
|   resolving-merge-conflicts-in-git.md    |             解决合并冲突              |
|                git-FAQ.md                |           git 常见问题解答            |





[download-git-st]: https://pan.baidu.com/s/1boULIiF
[git-guide]: http://rogerdudler.github.io/git-guide/index.zh.html
[git-tutorial-by-lxf]: http://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000/