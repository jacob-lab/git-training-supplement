# Resolving Merge Conflicts in Git

基于 `master` 新建分支 `feature1`

![2017-05-18 at 14.27](http://ww2.sinaimg.cn/large/006tNc79gy1ffpjbh03fzj30o409mmyb.jpg)



在 `feature1` 分支，继续我们的开发工作。修改文件 `no.ff` ，添加一行内容，如下图所示：

![2017-05-18 at 14.32](http://ww3.sinaimg.cn/large/006tNc79gy1ffpjbgky21j30nc05ddgf.jpg)



将 `feature1` 分支的修改的内容提交到 `commit history`

```shell
➜  learngit git:(feature1) ✗ git add no.ff
➜  learngit git:(feature1) ✗ git commit -m "add 'and simple'"
[feature1 2677f6a] add 'and simple'
 1 file changed, 1 insertion(+)
```


`feature1` 分支开发工作完成



------



切回 `master` 分支，同样修改文件 `no.ff`，添加内容如下图所示：

![2017-05-18 at 14.34](http://ww1.sinaimg.cn/large/006tNc79gy1ffpjbhotfgj30lf07jq3i.jpg)



提交 `master` 分支的修改内容

```shell
➜  learngit git:(master) ✗ git add no.ff
➜  learngit git:(master) ✗ git commit -m "add '& simple'"
[master 3a58dc1] add '& simple'
 1 file changed, 1 insertion(+)
```


`master` 分支开发工作完成

​

-----



###  1. 查看分支

现在 `master` 分支和 `feature1` 分支各自都分别有新的提交，如下图所示：

![2017-05-18 at 14.36](http://ww1.sinaimg.cn/large/006tNc79gy1ffpjbelq6lj30ex05faaf.jpg)



### 2. 合并分支

当 `master` 和 `feature1` 均有提交时，`git` 无法执行`快速合并`，只能试图把各自的修改合并起来，但这种合并就可能会有冲突。我们现在尝试将 `feature1` 合并到 `master`：

![2017-05-18 at 14.36](http://ww4.sinaimg.cn/large/006tNc79gy1ffpjbhxw00j30do07u0ti.jpg)



### 3. 产生冲突

果然冲突了！`Git` 告诉我们，必须手动解决冲突后再提交。

![2017-05-18 at 14.37](http://ww4.sinaimg.cn/large/006tNc79gy1ffpjbg795ij30lj0akwga.jpg)



### 4. 解决冲突

`no.ff` 冲突状态，`git` 用 `<<<<<<<`，`=======`，`>>>>>>>` 标记出不同分支的内容。

`<<<<<<< HEAD` 与 `=======` 之间的是当前分支的内容

`=======` 与 `>>>>>>> feature1`之间的内容属于 `feature1` 分支

![2017-05-18 at 14.38](http://ww4.sinaimg.cn/large/006tNc79gy1ffpjbjhastj30me094758.jpg)



我们修改如下：

![2017-05-18 at 14.53](http://ww3.sinaimg.cn/large/006tNc79gy1ffpjbihx5tj30m4094gmf.jpg)



再提交，将 `Unstaged files` 的文件提交到 `Staged files`。如果是命令行，执行 `git add no.ff`即可。

![2017-05-18 at 14.55](http://ww1.sinaimg.cn/large/006tNc79gy1ffpjbf89aqj30ln09ogma.jpg)



提交本次修改，如果是命令行，执行：`git commit` 提交到 `commit history`

![2017-05-18 at 14.56](http://ww3.sinaimg.cn/large/006tNc79gy1ffpjbixfryj30ly0evdhe.jpg)



至此，合并和解决冲突工作完成。

![2017-05-18 at 14.58](http://ww2.sinaimg.cn/large/006tNc79gy1ffpjces0k1j30qb0eu0v1.jpg)



## 小结

当 `git` 无法自动合并分支时，必须先解决冲突。解决冲突后，再提交`（git add && git commit）`，合并完成。
