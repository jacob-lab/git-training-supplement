## 前言

**以下所有操作凡是涉及个人信息，如姓名和邮箱，需要修改为自己的信息。**

**以下所有操作凡是涉及个人信息，如姓名和邮箱，需要修改为自己的信息。**

**以下所有操作凡是涉及个人信息，如姓名和邮箱，需要修改为自己的信息。**

**重要事情说三遍！！！**



## 安装 Git

* 官网下载 [msysgit][download-msysgit-url]（网速慢的童鞋请移步[国内镜像][download-git-st] ，提取密码: 87jk）
* 默认选项安装即可



## 设置 Git 的配置信息

安装完成后，在开始菜单里找到 "Git" -> "Git Bash"。

```shell
$ git config --global user.name "Your Name"
$ git config --global user.email "email@example.com"
```

**--global** 表示本台计算机所有的 Git 仓库都会使用这个配置。

![git-config](http://ww1.sinaimg.cn/large/006tNbRwly1ffim49hwr6j30h4094js0.jpg)



## 生成 ssh 密钥对

"Git Bash" 输入`ssh-keygen -t rsa -C "email@example.com"`，一路回车确认即可。

![ssh-keygen](http://ww3.sinaimg.cn/large/006tNbRwly1ffim48x4hfj30gq0dgq5p.jpg)



## 保存公钥到远端服务器

### Gitlab

注意：大部分公司会部署私有的 Gitlab 服务，Gitlab 相关账号需要自行向公司申请！！

### ![gitlab-ssh](http://ww4.sinaimg.cn/large/006tNbRwly1ffim4aknekj30ym0egac4.jpg)

> 以下出现 `PORT` 和 `GITLAB_SERVER_URL` 的地方，请自行替换为贵公司 `Gitlab` 服务器访问端口和地址。
>
> `GITLAB_SERVER_URL` 和 `PORT` 可以从具体项目的 `SSH` 地址获取，如 `ssh://git@xxx.bbb.com:9999/hello/world.git`，`git@xxx.bbb.com` 为 Gitlab 服务器访问地址，`9999` 为访问端口。如果项目的 SSH 地址没有显示端口号，表示使用默认端口 `22`。

在终端 `Git Bash` 输入命令 `ssh -T -p PORT git@GITLAB_SERVER_URL` 验证公钥配置结果。

回车后返回 `Welcome to GitLab, 你的名字!` 说明配置成功，否则失败。

如果 Gitlab 服务器的端口号为默认值 `22`，可以将命令缩减为 `ssh -T git@gitlab_server_url`。



### 码云（oschina）

[Adding a new SSH key to oschina][oschina-ssh]

在终端 `Git Bash` 输入命令 `ssh -T git@git.oschina.net` 验证公钥配置结果。

回车后返回 `Welcome to Git@OSC, 你的名字!` 说明配置成功，否则失败。



### Github

[Adding a new SSH key to your GitHub account][github-ssh]

[Testing your SSH connection][test-ssh-github]

在终端 `Git Bash` 输入命令 `ssh -T git@github.com` 验证公钥配置结果。

回车后返回 `Hi 你的名字! You've successfully authenticated, but GitHub does not provide shell access.` 说明配置成功，否则失败。



## FAQ

[Frequently Asked Questions About Git][git-FAQ]




[download-msysgit-url]: https://git-for-windows.github.io/
[oschina-ssh]: http://git.mydoc.io/?t=154712
[github-ssh]: https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/
[test-ssh-github]: https://help.github.com/articles/testing-your-ssh-connection/
[download-git-st]: https://pan.baidu.com/s/1boULIiF
[git-FAQ]: http://git.oschina.net/buildyourdream/git-training-supplement/blob/master/git-FAQ.md

