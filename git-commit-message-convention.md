**commit message：提交说明，应该清晰明了，说明本次提交的目的**



## 作用

- 提供更多的历史信息，加快 reviewing code 的过程
- 可以过滤某些 commit（比如文档改动），便于快速查找信息
- 可以直接从 commit 生成 change log
- 让其他开发者运行 git blame 的时候想跪谢
- 好的提交信息，会帮助你提高项目的整体质量



## 格式

每次提交，Commit message 都包括三个部分：Header，Body 和 Footer。

```
<type>: <subject>
// 空一行
<body>
// 空一行
<footer>
```
其中，Header 是必需，Body 和 Footer 可省

注：一行都不得超过72个字符（或100个字符），为了避免自动换行影响美观。



--



### Header

Header部分只有一行，包括两个字段：
type（必需）
subject（必需）

- type  
    用于说明 commit 的类别，只允许使用下面 8 个标识。
    * feat：新功能（feature）
    * fix：bugs fixed
    * change: functionality changed
    * docs：文档（documentation）
    * style：格式（不影响代码运行的变动）
    * refactor：重构（即不是新增功能，也不是修改bug的代码变动）
    * test：增加测试
    * chore：构建过程或辅助工具的变动

- subject  
    subject 是 commit 目的的简短描述，不超过50个字符。

注：

- 以动词开头，使用第一人称现在时，比如change，而不是changed或changes
- 第一个字母小写
- 结尾不加句号

### Body

对本次 commit 的详细描述，可以分成多行

### Footer

只用于两种情况:不兼容变动、关闭 Issue

### Revert

特殊情况，如果当前 commit 用于撤销以前的 commit，则必须以revert:开头，后面跟着被撤销 Commit 的 Header。


--


更详细的内容请点击原文：[Git提交的正确姿势] [commit message guide]

好文推荐：[写出好的 commit message] [writing good commit message]

[commit message guide]: http://www.oschina.net/news/69705/git-commit-message-and-changelog-guide
[writing good commit message]: https://ruby-china.org/topics/15737

