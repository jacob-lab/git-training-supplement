# Frequently Asked Questions About Git



## Q1

> error: RPC failed; HTTP 413 curl 22 The requested URL returned error: 413 Request Entity Too Large

* Why：

  413 errors occur when the request body is larger than the server is configured to allow. Also happens when you do a HTTP push over Nginx.

* How to fix：

  change protocol from https to ssh.